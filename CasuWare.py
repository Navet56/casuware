"""
CasuWare
Version 1.0
Author : Navet56
License : GPL v3
Needs : Python 3.8, notify2, tkinter
"""

import notify2
import os
from tkinter import *
from multiprocessing import Pool
import time

title="CasuWare"
osname = os.popen("lsb_release -d | sed 's/Description:/ /'").read().strip()
thereIsUpdates = False
global majPop
majPop = True
save = open('data/save.csave', 'r')
saveString = save.read()
save.close()
if saveString == "False":
    majPop = False

class System(object):
    def __init__(self, name):
        self.name = name
        if name == "manjarokde":
            self.soft = "pamac-manager"
            self.screen = "kcmshell5 kcm_kscreen&"
            self.settings = "systemsettings5&"
            self.sound = "kcmshell5 kcm_pulseaudio&"
            self.update = "pamac-manager --updates"
        elif name == "redhat":
            self.soft = "gnome-software"
            self.screen = ""
            self.settings = ""
            self.sound = ""
            self.update = ""
        elif name == "mint":
            self.soft = ""
            self.screen = ""
            self.settings = ""
            self.sound = ""
            self.update = "mint-update"
        else:
            self.soft = "gnome-software"
            self.screen = ""
            self.settings = "gnome-settings"
            self.sound = ""
            self.update = "gnome-software"

if "Manjaro" in osname:
    syst = System("manjarokde")
elif "int" in osname:
    syst = System("mint")
elif ("edora" in osname) or ("SUSE" in osname) or ("Cent" in osname) or ("at" in osname):
    syst = System("redhat")
else:
    syst = System("ubuntu")

def fileCommand():
    os.system("xdg-open ~")

def updateOSCommand():
    os.system(syst.update)

def updatePop():
    global majPop
    majPop = not majPop
    save = open('data/save.csave', 'w')
    save.write(str(majPop))
    save.close()

def soft():
    os.system(syst.soft)

def screenCommand():
    os.system(syst.screen)

def soundCommand():
    os.system(syst.sound)

def settingCommand():
    os.system(syst.settings)

def webCommand():
    os.system("xdg-open google.fr")

def infosSystem():
    print(osname)

def gui():
    print(title)
    win = Tk()
    win.title(title)
    win.geometry("700x900")
    Grid.columnconfigure(win, index=0, weight=1)
    update = PhotoImage(file = r"data/update.png").subsample(10, 10) 
    screen = PhotoImage(file = r"data/screen.png").subsample(10, 10) 
    sound = PhotoImage(file = r"data/sound.png").subsample(11, 11) 
    web = PhotoImage(file = r"data/web.png").subsample(12, 12)
    files = PhotoImage(file = r"data/file.png").subsample(5, 5) 
    settings = PhotoImage(file = r"data/settings.png").subsample(10, 10) 
    plus = PhotoImage(file = r"data/plus.png").subsample(10, 10) 
    apps = PhotoImage(file = r"data/apps.png").subsample(10, 10) 
    buttonUpdate = Button(win, text="Mise à jour", bg='white', fg='black', image = update, compound = LEFT , command=updateOSCommand)
    buttonSofts = Button(win, text="Logiciels", bg='white', fg='black' ,image = apps, compound = LEFT ,command=soft)
    buttonSeeMore = Button(win, text="Informations système", bg='white', fg='black' ,image = plus, compound = LEFT ,command=infosSystem)
    buttonScreen = Button(win, text="Affichage", bg='white', fg='black' ,image = screen, compound = LEFT ,command=screenCommand)
    buttoSound = Button(win, text="Son", bg='white', fg='black' ,image = sound, compound = LEFT ,command=soundCommand)
    buttonSetting = Button(win, text="Paramètres",bg='white', fg='black' ,image = settings, compound = LEFT ,command=settingCommand)
    buttonWeb = Button(win, text="Internet",bg='white', fg='black' ,image = web, compound = LEFT ,command=webCommand)
    buttonFile = Button(win, text="Fichiers",bg='white', fg='black' ,image = files, compound = LEFT ,command=fileCommand)

    r = 1
    buttons = [buttonUpdate, buttonWeb, buttonFile, buttonSofts, buttonSetting, buttoSound, buttonScreen, buttonSeeMore]
    for b in buttons:
        b.grid(column=0, row=r, sticky=N+S+E+W)
        Grid.rowconfigure(win, r, weight=1)
        r+=1
    Checkbutton(win, text="Afficher les mises à jour lorsqu'il y en a de disponible", command=updatePop, fg='black', bg='white',  height=3).grid(row=r, sticky=N+S+E+W)

    def resize(e):
        fontSize = int(e.width/20)
        if thereIsUpdates:
            buttonUpdate.text = "MISE A JOUR DISPO !"
        for b in buttons:
            b.config(font=("Helvetica", fontSize))
    
    win.bind('<Configure>', resize)
    win.eval('tk::PlaceWindow . center')
    win.mainloop()

def checkUpdate():
    print("check updates ...")
    checkupdates = os.popen("checkupdates").read()
    if checkupdates > "" : 
        thereIsUpdates = True

    if thereIsUpdates :
        notify2.init(title)
        notify2.Notification(title, "Vous avez une nouvelle mise à jour !", "image").show()
        if not majPop :
            updateOSCommand()

# Création des threads
pool = Pool(processes=2) #TODO use Process
r1 = pool.apply_async(checkUpdate)
r2 = pool.apply_async(gui)
pool.close()
pool.join()

